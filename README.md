# Configstore

Easily load and persist config without having to think about where and how.
Port of the Node.js module found at https://github.com/yeoman/insight.
