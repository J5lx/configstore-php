<?php namespace J5lx\Configstore\Tests;

use J5lx\Configstore\Configstore;

class ConfigstoreTest extends \PHPUnit_Framework_TestCase
{
    protected static $configstorePath = '';
    protected $conf = null;

    public static function setUpBeforeClass()
    {
        self::$configstorePath = (new Configstore('configstore-test'))->path;
    }

    protected function setUp()
    {
        @unlink($this::$configstorePath);
        $this->conf = new Configstore('configstore-test');
    }

    /**
     * @covers J5lx\Configstore\Configstore::set
     * @covers J5lx\Configstore\Configstore::get
     * @uses J5lx\Configstore\Configstore::__construct
     * @uses J5lx\Configstore\Configstore::__set
     * @uses J5lx\Configstore\Configstore::__get
     */
    public function testSetAndGet()
    {
        $this->conf->set('foo', 'bar');
        $this->assertEquals('bar', $this->conf->get('foo'));
    }

    /**
     * @covers J5lx\Configstore\Configstore::del
     * @uses J5lx\Configstore\Configstore::set
     * @uses J5lx\Configstore\Configstore::get
     * @uses J5lx\Configstore\Configstore::__construct
     * @uses J5lx\Configstore\Configstore::__set
     * @uses J5lx\Configstore\Configstore::__get
     */
    public function testDel()
    {
        $this->conf->set('foo', 'bar');
        $this->conf->del('foo');
        $this->assertEquals('', $this->conf->get('foo'));
    }

    /**
     * @covers J5lx\Configstore\Configstore::__get
     * @uses J5lx\Configstore\Configstore::set
     * @uses J5lx\Configstore\Configstore::__construct
     * @uses J5lx\Configstore\Configstore::__set
     */
    public function testAll()
    {
        $this->conf->set('foo', 'bar');
        $this->assertEquals('bar', $this->conf->all['foo']);
    }

    /**
     * @covers J5lx\Configstore\Configstore::__get
     * @uses J5lx\Configstore\Configstore::set
     * @uses J5lx\Configstore\Configstore::__construct
     * @uses J5lx\Configstore\Configstore::__set
     */
    public function testSize()
    {
        $this->conf->set('foo', 'bar');
        $this->AssertEquals(1, $this->conf->size);
    }

    public function testPath()
    {
        $this->conf->set('foo', 'bar');
        $this->assertFileExists($this->conf->path);
    }

    /**
     * @covers J5lx\Configstore\Configstore::__construct
     * @uses J5lx\Configstore\Configstore::get
     * @uses J5lx\Configstore\Configstore::__set
     * @uses J5lx\Configstore\Configstore::__get
     */
    public function testUseOfDefaultValue()
    {
        $conf = new Configstore('configstore-test', ['foo' => 'bar']);
        $this->assertEquals('bar', $conf->get('foo'));
    }

    public function testAllBeingAlwaysAnArray()
    {
        unlink($this::$configstorePath);
        $this->assertInternalType('array', $this->conf->all);
    }
}
