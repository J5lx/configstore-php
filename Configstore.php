<?php namespace J5lx\Configstore;

use Phine\Path\Path;
use XdgBaseDir\Xdg;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

class Configstore
{
    private $defaultPathMode = 0700;
    private $defaultFileMode = 0600;
    private $permissionError = 'You don\'t have access to this file.';

    public $appConfDir = '';
    public $path       = '';

    public function __construct($id, Array $defaults = [])
    {
        $xdg      = new Xdg();
        $userinfo = posix_getpwuid(posix_geteuid());
        $user     = $userinfo['name'];
        $userDir  = $xdg->getHomeDir();
        if (is_writable($xdg->getHomeConfigDir())) {
            $configDir = $xdg->getHomeConfigDir();
        } else {
            $configDir = Path::join([sys_get_temp_dir(), $user, '.config']);
        }

        $this->appConfDir = Path::join([$configDir, 'configstore']);
        $this->path       = Path::join([$this->appConfDir, $id . '.yml']);

        $this->all = array_merge($defaults, $this->all);
    }

    public function __get($name)
    {
        switch ($name) {
            case 'all':
                if (!file_exists($this->path)) {
                    if (!is_dir($this->appConfDir)) {
                        mkdir($this->appConfDir, $this->defaultPathMode, true);
                    }
                    return [];
                }
                if (!is_readable($this->path)) {
                    throw new \RuntimeException($this->permissionError);
                }

                $file = file_get_contents($this->path);
                if ($file === false) {
                    throw new \RuntimeException(
                        "An unknown error occured while reading the config file"
                    );
                }

                try {
                    return Yaml::parse($file);
                } catch (ParseException $e) {
                    file_put_contents($this->path, '');
                    return [];
                }
                return;
            case 'size':
                return count($this->all);
        }
        return null;
    }

    public function __set($name, Array $value)
    {
        if ($name == 'all') {
            // make sure the folder exists, it could have been deleted meanwhile
            if (!is_dir($this->appConfDir)) {
                mkdir($this->appConfDir, $this->defaultPathMode, true);
            }

            $result = file_put_contents($this->path, Yaml::dump($value));
            if ($result === false) {
                throw new \RuntimeException(
                    "An unknown error occured while writing to the config file"
                );
            }
            chmod($this->path, $this->defaultFileMode);
        }
    }

    public function get($key)
    {
        $config = $this->all;
        if (!isset($config[$key])) {
            return '';
        }
        return $config[$key];
    }

    public function set($key, $value)
    {
        $config       = $this->all;
        $config[$key] = $value;
        $this->all    = $config;
    }

    public function del($key)
    {
        $config = $this->all;
        unset($config[$key]);
        $this->all = $config;
    }
}
